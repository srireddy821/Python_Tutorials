"""
 Numpy tutorials
 http://mathesaurus.sourceforge.net/matlab-numpy.html
 http://scipy.github.io/old-wiki/pages/NumPy_for_Matlab_Users.html
"""
import numpy as np

def np_examples():
    """
    Array for 1xN, Nx1, N tensors storage
    All operations are meant for element wise operations
    * - elementwise product
    dot - multiplication
    .T - transpose
    A[:, 1] - Access all elements of specificed dimension
    """

    # a = [1 2 3; 4 5 6]
    a = array([[1., 2., 3.], [4., 5., 6.]])
    
    # ndims
    ndim(a)
    
    # numel
    size(a)
    
    # size
    shape(a)
    
    # a(1)
    a[0]
    
    # a(end)
    a[-1]
    
    #a .* b
    a * b
    
    #a(2, :)
    a[2, :]
    
    # zeros(3, 4)
    zeros((3, 4))
    
    # b = a
    b = a.copy() 

    # [a b]
    concatenate((a,b))
    
    """
    Matrix for linear algebra operations
    *  - matrix multiplication
    multiply - element wise multiplication
    .H, .I, .T - conjugate, inverse, transpose
    """
    
    a = mat([[1.,2.,3.],[4.,5.,6.]]) 
    b = mat("1 2 3; 4 5 6")
    
    #bmat = [a b; c d]
    bmat('a b; c d')
    
    # Conjugate
    a.H
    
    # Matrix multiply
    a*b
    
    # Elementwise
    multiply(a, b)


if __name__ == "__main__":
    np_examples()
    print "Numpy Tutorial"
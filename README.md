## Python Course

http://stanfordpython.com/#lecture


## Python Environment Setup

Install PyCharm community free edition below location 

1. https://www.jetbrains.com/pycharm/download/
2. Watch PyCharm getting started videos (1,2,3) at : https://www.jetbrains.com/pycharm/documentation/
3. Start new project 
4. Copy code from lab1.py
5. Put break points and interactively try out each section

## Week 1 

1. Finish first 3 lessons at http://stanfordpython.com/#lecture
2. Check ''lab1.py'' code to get familarize with data structures and fundamentals 

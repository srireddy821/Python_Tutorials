"""
 This tutorial covers functions and passing arguments and importing functions from other packages
"""

"""
    Method  example
"""
def my_method():
    print "Hello World"


"""
    Class example
"""
class Calculator(object):

    def __init__(self):
        self.value = "Some Value"

    def add(self, a, b):
        result = a + b
        return result

"""
  Class inhertence, static method
"""

"""
    Abstract class
"""
from abc import ABCMeta, abstractmethod
class Vehicle(object):
    """A vehicle for sale by Jeffco Car Dealership.


    Attributes:
        wheels: An integer representing the number of wheels the vehicle has.
        miles: The integral number of miles driven on the vehicle.
        make: The make of the vehicle as a string.
        model: The model of the vehicle as a string.
        year: The integral year the vehicle was built.
        sold_on: The date the vehicle was sold.
    """

    __metaclass__ = ABCMeta

    base_sale_price = 0

    @abstractmethod
    def vehicle_type(self):
        """"Return a string representing the type of vehicle this is."""
        pass

    @staticmethod
    def min_speed():
        return 100

class Car(Vehicle):

    wheels = 4

    def __init__(self, make, model):
        print make
        self.make = make
        self.model = model

    def vehicle_type(self):
        return self.make

    def add_price(self, a, b):
        print a + b
        return a + b


if __name__ == "__main__":
    car = Car('mustang', 2016)
    car.add_price(10, 20)
    print car.vehicle_type()
    print Car.min_speed()

    calc = Calculator()
    result = calc.add(10, 20)

    print result
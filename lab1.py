"""
 This tutorial covers basic data structures, control statement

 Fundamentals: https://drive.google.com/file/d/0B-eHIhYpHrGDaFVLZnRxVGg1RWc/view
 Data structures: https://drive.google.com/file/d/0B6rwNya1p7r4TGo1eXhkbWpXVHM/view
"""

print "Hello World"


# Single line comment

""" Multi
      line
        comment """

# In python Indexing starts from 0

"""
    Strings
"""
# Strings
str1 = 'Python Intro'
print str1[0]
str2 = "b"
str3 = str1+str2
print("  %s --   %s -- %s " % (str1, str2, str3))
print len(str1)

"""
    Numbers
"""
# operator  + - * / ** (power)
# Numbers
int_n  = 10
float_n = 2.71
print int_n**2


"""
 Labs
"""
# Lists : Contains elements of same data type
list  = [66.25, 333, 333, 1, 1234.5] # Array of numbers
list.reverse() # Removes last elements in an array
list.append(1000)
list.pop()


# arrayfun on lists
x = [0, 1, 2, 3, 4, 5]
squares = [el**2 for el in x]

squares = [el**2 for el in range(10)]

# Creating an empty list and appending elements
combs = []
for x in [1,2,3]:
    for y in [3,1,4]:
        if x != y:
            combs.append((x, y))

# Deleteting element in list
a = [1, 2, 3, 4]
# Removes second element from array
del a[1]

"""
 Tuples - collection of immutable, heterogeneous set elements, can be nested
"""
# Create immutable tuple element
t = 12345, 54321, 'hello!'

print t

# Unpack to individual elements
x, y, z  = t

# Pack multiple tuples
c = 800, t
print c
((800), (12345, 54321, 'hello!'))

"""
 Dictionaries: Mainly used to store Key-Value pairs - Acts as hash map
 e.g Find a person complete  information based on SSN
"""

# Empty dict
edict = {}
# Initialize directly with KV pairs
c = dict({'sape': 4139, 'guido': 4127, 'jack': 4098})
# Initialize it with array of tuples
a = dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
# Intialize with KV pairs
b = dict(sape=4139, guido=4127, jack=4098)

a == b
b == c

# Print value of a given key
print c['sape']
# Update value of a specific key
c ['guido'] = 100
# Delete key-value pair in dictionary
del c ['jack']

# Loop over Dictionary and print all Key value pairs
knights = {'gallahad': 'the pure', 'robin': 'the brave'}
for key, val in knights.items():
     print(key, val)

for key, val in enumerate(['tic', 'tac', 'toe']):
    print(key, val)



"""
 Set: A set is an unordered collection with no duplicate elements.
 Set objects also support mathematical operations like union, intersection, difference,
 and symmetric difference
"""
# Create empty set
a = set()

# Create a set with elements, initilization removes duplicates
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}

# Check membership of element
print 'orange' in basket

a = set('abracadabra')
b = set('alacazam')
# Set operations
print a - b
print a & b
print a | b
print a ^ b

# Set supports comprehension too
a = {x for x in 'abracadabra' if x not in 'abc'}
print a

# ** In Python, blocks of code are defined using indentation. (careful about indentation)

traffic_light = "Hello"
# Syntax for control statements
if traffic_light == "green":
    print 'green'
elif traffic_light == "orange":
    print 'orange'
else:
    print "Red"

# For loop syntax
# Range is equivalent to 1:5 in M - In python 0:4
for i in range(5):
    print i**2

# Break and continue statements in code
# Get prime numbers in range to 10
max_n = 10
for n in range(1, max_n):
    if n == 1:
        continue

    for x in range(2, n):
        if n % x == 0:
            print n, "equals", x, "*", n/x
            break
        else:
            print n

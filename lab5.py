"""
 Pandas syntax
 http://pandas.pydata.org/pandas-docs/stable/10min.html
 http://pandas.pydata.org/pandas-docs/stable/remote_data.html
 http://www.slideshare.net/continuumio/data-pandas-profit
 
 Exercise:
 http://nullege.com/codes/search/pandas.io.data
"""

import pandas as pd
import numpy as np
from pandas import DataFrame
import datetime
import pandas.io.data
import matplotlib.pyplot as plt

"""
Series    - A series is a one-dimensional NumPy-like array. You can put any data type in here, and perform vectorized operations on it. 
            A series is also dictionary - like in many ways. Usually this is denoted as "s."
DataFrame - Two-dimensional NumPy-like array. Again, any data type can be stuffed in here. Usually this is denoted as "df".
Index     - This is what the data is "associated" by. So if you have time series data, like stock price information, generally the "index" is the date.
Slicing   - Selecting specific batches of data.

CSV, EXCEL, HDF5
"""

def getdata_from_yahoo(name='AAPL'):
    # Download the data from Yahoo
    aapl = pd.io.data.get_data_yahoo('AAPL', start=datetime.datetime(2000, 10, 2), end=datetime.datetime(2016, 6, 1))
    # Print few lines of data
    print(aapl.head())
    # Save data to CSV file
    aapl.to_csv('aapl.csv');
    
    # Read CSV file for operations
    df = pd.read_csv('aapl.csv', index_col='Date', parse_dates=True)
    print(df.head())
    
    # Read selected coumns of data frame
    df2 = df[['Close','High']]
    print(df2.head())
    
    # Apply compartator operation on specific column of data frame
   df3 = df[(df['Close'] > 1400)]
   print(df3)
   
   # Apply math operations between two columns
   df['H-L'] = df.High - df.Low
   print(df.head())
   
   # Rolling mean of numbers of column in data frame
   df['100MA'] = pd.rolling_mean(df['Close'], 100)
   print(df.head())
   
   # Find diff between two consecutive elements
   df['diff'] = df['Close'].diff()
   print(df.head())
   
   # Plot the data in data frames
   df[['Open','High','Low','Close','100MA']].plot()
   plt.show()
   
   # Summarize all data columns in the data frames
   # Number of data points, mean, min, max, standara deviation, 25%, 50%, 75% number of items deviated from mean
   print(df.describe())
   
   # Correlation
   print(df.corr())
   
   # Covariannce
   print(df.cov())
   
   # Remove column from data
   del aapl['Close']
   
   
def pandas_dataframe_examples():
    
    # Create a data frame of 6 rows with columns named as A, B, C, D
    df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list('ABCD'))
    
    # Get summary of data, column names, number of data points and data type
    print(df)
    
    len(df)
    
    print(df.types)
    
    print(df.columns)
    
    df.describe()
    
    # Print first 10 elements of mean_temp column
    print(df.mean_temp.head(10))
    
    # Print last 10 elements
    print(df.tail(10))
    
    # Find standard deviation
    df.std()
    
    # plots histogram
    df.mean_temp.hist()
    
    # Filter data
    filtered_data = df[df.max_temp <= 32]
    
    # Get each column name
    df.apply(lambda c: c.name)    
    
    # axis = 1 specifies that lambda function should go over each row of data
    df.apply(lambda r: r["max_pressure"] - r["min_pressure"], axis=1)


def pandas_timeseries_examples():
    ts = pd.Series(np.random.randn(1000), index=pd.date_range('1/1/2000', periods=1000))
    ts = ts.cumsum()
    ts.plot()
    
    
if __name__ == "__main__":
    pandas_data_frame_examples()
    print "Numpy Tutorial"
